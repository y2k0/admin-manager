const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function (app) {
  app.use(
    '/bdApi',
    createProxyMiddleware({
      // target: 'http://api.map.baidu.com/telematics/v3/weather?location=shenzhen&output=json&ak=3p49MVra6urFRGOT9s8UBWr2'
      target: 'http://api.map.baidu.com',
      changeOrigin: true,
      pathRewrite: {
        '/bdApi': ''
      }
    })
  )
}
