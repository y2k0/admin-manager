import React from 'react'
import { Link } from 'react-router-dom'

class Main extends React.Component {
  render() {
    return (
      <div>
        This is Main Page
        {/* update 2020.11.23 嵌套路由 */}
        <Link to="/main/ach">嵌套路由</Link>
        <hr />
        {/* update 2020.11.23 新增子组件 */}
        {this.props.children}
      </div>
    )
  }
}

export default Main
