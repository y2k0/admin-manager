import React from 'react'

import { Link } from 'react-router-dom'

class Home extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to="/main">Home02</Link>
          </li>
          <li>
            <Link to="/about">About02</Link>
          </li>
          <li>
            <Link to="/topics">Topics02</Link>
          </li>
        </ul>
        <hr />
        {this.props.children}
      </div>
    )
  }
}

export default Home
