import React from 'react'

// react-router v5
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom'

import Main from './Main'
import About from './About'
import Topics from './Topics'

class Home extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/topics">Topics</Link>
            </li>
          </ul>
          <hr />
          {/* Switch 只渲染第一个匹配到的路由  exact 精准匹配 */}
          <Switch>
            <Route exact path="/">
              <Main />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/topics" component={Topics}></Route>
          </Switch>
        </div>
      </Router>
    )
  }
}

export default Home
