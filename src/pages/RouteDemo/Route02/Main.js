import React from 'react'

class Main extends React.Component {
  render() {
    return (
      <div>
        This is Main Page
        <hr />
        {/* update 2020.11.23 新增子组件 */}
        {this.props.children}
      </div>
    )
  }
}

export default Main
