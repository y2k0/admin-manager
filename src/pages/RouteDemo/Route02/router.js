import React from 'react'
import { HashRouter as Router, Route, Link } from 'react-router-dom'

import Main from './Main'
import About from './About'
import Topics from './Topics'

import Home from './Home'

class IRouter extends React.Component {
  render() {
    return (
      <Router>
        <Home>
          {/* update 2020.11.23 路由中加载子组件调整 使用 render 属性 */}
          {/* <Route exact path="/" component={Main}></Route> */}
          <Route
            exact
            path="/"
            render={() => (
              <Main>
                <div>这是一个子组件</div>
              </Main>
            )}
          ></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/topics" component={Topics}></Route>
        </Home>
      </Router>
    )
  }
}

export default IRouter
