import React from 'react'
import { Link } from 'react-router-dom'

class Main extends React.Component {
  render() {
    return (
      <div>
        This is Main Page
        <br />
        {/* update 2020.11.23 嵌套路由 */}
        <Link to="/main/test-id">嵌套路由1</Link>
        <br />
        <Link to="/main/245">嵌套路由2</Link>
        <hr />
        {/* update 2020.11.23 新增子组件 */}
        {this.props.children}
      </div>
    )
  }
}

export default Main
