import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'

import Main from './Main'
import About from './About'
import Topics from './Topics'
import Info from './Info'
import NoMatch from './NoMatch'

import Home from './Home'

class IRouter extends React.Component {
  render() {
    return (
      <Router>
        <Home>
          {/*
            update 2020.11.23 新增子路由调整 使用 render 属性； 移除 exact 属性；
            由于 ="/" 在没有 exact 的限定下，该路由始终被匹配到，而当前子路由案例是直接在这个路由之下的，
            所以这里调整下父级路由的 path="/main" ，保证正常匹配；同时外层可以包裹一层 Switch 组件，保证
            匹配到了一个，就不再继续匹配了
          */}
          {/* <Route exact path="/" component={Main}></Route> */}
          <Switch>
            <Route
              path="/main"
              render={() => (
                <Main>
                  <Route path="/main/:mainId" component={Info}></Route>
                </Main>
              )}
            ></Route>
            <Route path="/about" component={About}></Route>
            <Route path="/topics" component={Topics}></Route>
            <Route component={NoMatch}></Route>
          </Switch>
        </Home>
      </Router>
    )
  }
}

export default IRouter
