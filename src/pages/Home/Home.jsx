import React from 'react'

import styles from './Home.module.less'

class Home extends React.Component {
  render() {
    return <div className={styles.content_wrap}>欢迎进入 Admin MS 后台管理系统学习课程</div>
  }
}

export default Home
