import React from 'react'
import styles from './NoMatch.module.less'

class NoMatch extends React.Component {
  render() {
    return (
      <div className={styles.no_match}>
        <img src="/404.svg" alt="404 no found" />
      </div>
    )
  }
}

export default NoMatch
