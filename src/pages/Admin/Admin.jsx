import React from 'react'
import { Row, Col } from 'antd'

import styles from './Admin.module.less'

import Header from '@components/Header'
import Footer from '@components/Footer'
import NavLeft from '@components/NavLeft'
import Home from '@/pages/Home'

/**
 * 管理系统主页
 */
export default class Index extends React.Component {
  render() {
    return (
      <>
        <Row className={styles.container}>
          <Col span={3} className={styles.nav_left}>
            <NavLeft />
          </Col>
          <Col span={21} className={styles.main}>
            <Header />
            <Row className={styles.main_container}>
              {/* <Home /> */}
              {this.props.children}
            </Row>
            <Footer />
          </Col>
        </Row>
      </>
    )
  }
}
