import React from 'react'

/**
 * 最外层组件
 * 可以接受一切组件
 * @extends React
 */
class App extends React.Component {
  render() {
    return <div> {this.props.children}</div>
  }
}

export default App
