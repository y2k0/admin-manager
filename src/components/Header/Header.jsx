import React from 'react'
import { Row, Col } from 'antd'
import styles from './Header.module.less'
import axios from 'axios'
import Utils from '@/utils/utils'

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    this.setState({
      userName: '五味子'
    })

    setInterval(() => {
      let sysTime = Utils.formatDate(new Date().getTime())
      this.setState({
        sysTime
      })
    }, 1000)

    this.getWeatherAPIData()
  }

  getWeatherAPIData() {
    let city = 'shenzhen'
    axios
      .get(`/bdApi/telematics/v3/weather?location=${encodeURIComponent(city)}&output=json&ak=3p49MVra6urFRGOT9s8UBWr2`)
      .then((res) => {
        console.log(res.data)
        if (res.data && res.data.status === 'success') {
          let data = res.data
          const { dayPictureUrl, weather } = data.results[0].weather_data[0]
          this.setState({
            dayPictureUrl,
            weather
          })
        } else {
          console.error(`请求失败`)
        }
      })
  }

  render() {
    return (
      <div className={styles.header}>
        <Row className={styles.header_top}>
          <Col span={24}>
            <span>欢迎，{this.state.userName}同学</span>
            <a className={styles.exit} href="#">
              退出
            </a>
          </Col>
        </Row>
        <Row className={styles.breadcrumb}>
          <Col span={4} className={styles.breadcrumb_title}>
            首页
          </Col>
          <Col span={20} className={styles.wether}>
            <span className={styles.date}>{this.state.sysTime}</span>
            <span className={styles.wether_detail}>
              <img className={styles.weather_icon} src={this.state.dayPictureUrl} alt="天气图片" />
              {this.state.weather}
            </span>
            {/*<span>
              <img className={styles.weather_icon} src={this.state.dayPictureUrl} alt="天气图片" />
            </span>*/}
          </Col>
        </Row>
      </div>
    )
  }
}

export default Header
