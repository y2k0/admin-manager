import React from 'react'

import styles from './Footer.module.less'

class Footer extends React.Component {
  render() {
    return <div className={styles.container}>版权所有：@五味子 &nbsp;&nbsp;&nbsp;&nbsp; 技术支持：五味子同学</div>
  }
}

export default Footer
