import React from 'react'
import { Menu } from 'antd'
import { NavLink } from 'react-router-dom'
import styles from './NavLeft.module.less'
import menuList from '@/config/menuConfig'

const { SubMenu } = Menu

class NavLeft extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuTreeNode: []
    }
  }

  componentDidMount() {
    const menuTreeNode = this.renderMenu(menuList)
    this.setState({
      menuTreeNode
    })
  }

  renderMenu = (data) => {
    return data.map((item) => {
      // 递归思想
      if (item.children) {
        // this.renderMenu(item.children)
        return (
          <SubMenu title={item.title} key={item.key}>
            {this.renderMenu(item.children)}
          </SubMenu>
        )
      }
      return (
        <Menu.Item title={item.title} key={item.key}>
          <NavLink to={item.key}>{item.title}</NavLink>
        </Menu.Item>
      )
    })
  }

  render() {
    return (
      <>
        <div className={styles.logo}>
          <img className={styles.logo_icon} src="/assets/logo-ant.svg" alt="" />
          <h1 className={styles.title}>Admin MS</h1>
        </div>
        <Menu theme="dark">
          {/*<SubMenu key="sub1" title="Navigation One">
            <Menu.Item key="1">Option 1</Menu.Item>
            <Menu.Item key="2">Option 2</Menu.Item>
            <Menu.Item key="3">Option 3</Menu.Item>
            <Menu.Item key="4">Option 4</Menu.Item>
          </SubMenu>*/}
          {this.state.menuTreeNode}
        </Menu>
      </>
    )
  }
}

export default NavLeft
