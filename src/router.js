import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import App from './App'
import Login from './pages/Login'
import Admin from './pages/Admin'
import NoMatch from './pages/NoMatch'
// UI
import Button from './pages/UI/Button'

/**
 * 给当前项目使用的路由管理组件
 * @extends React
 */
class IRouter extends React.Component {
  render() {
    return (
      <Router>
        <App>
          <Route path="/login" component={Login}></Route>
          <Route
            path="/admin"
            render={() => (
              <Admin>
                <Switch>
                  <Route path="/admin/ui/buttons" component={Button}></Route>
                  <Route component={NoMatch}></Route>
                </Switch>
              </Admin>
            )}
          ></Route>
          <Route path="/order/detail" component={Login}></Route>
        </App>
      </Router>
    )
  }
}

export default IRouter
