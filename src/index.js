import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
// import App from './pages/Admin'
// import Home from './pages/RouteDemo/Route02/Home'
// import Router from './pages/RouteDemo/Route02/router'
// import Router from './pages/RouteDemo/Route03/router'
// import Router from './pages/RouteDemo/Route04/router'

import Router from './router'

// import reportWebVitals from './reportWebVitals'

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <Home /> */}
    <Router />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals()
