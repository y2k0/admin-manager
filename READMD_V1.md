### create project

```shell
yarn create react-app admin-manager
```

### react-app-rewire

通常不建议直接 `eject`，如果我们需要扩展我们的 `webpack.config.js` ，则可以通过 `react-app-rewire` 包来进行增强扩展。

`react-app-rewire` 是对 `react-scripts` 进行了扩展

```shell
# babel-plugin-import 是用来实现按需打包的
yarn add babel-plugin-import react-app-rewire customize-cra -D
```

### 按需打包

如果通过 `eject` 的方式，则其实现方式就是在暴露的配置文件中增加如下实现代码：

```js
{
  "plugins": [
    "import",
    {
      "libraryName": "antd",    // 需要按需打包的库
      "libraryDirectory": "es",
      "style": "css"
    }
  ]
}
```

通过 `react-app-rewire` 来扩展配置的方式，则有所不同，具体如下:

第一步 在项目更目录下创建 `config-overrides.js` 文件，也可以创建 `config-overrides` 目录。在 `config-overrides.js` 文件新增如下内容：

```js
const { injectBabelPlugin } = require('react-app-rewire')
module.exports = function override(config, env) {
  config = injectBabelPlugin(
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css'
      }
    ],
    config
  )
  return config
}
```

```js
// 引入 react-app-rewired 并修改 package.json 里的启动配置。。
// 由于新的 react-app-rewired@2.x 版本的关系，你需要还需要安装 customize-cra。
// 需要安装 customize-cra  less less-loader
const { override, fixBabelImports, addLessLoader } = require('customize-cra')

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: 'css'
  }),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true
    }
  })
)
```

第二步 修改启动脚本

```js
"scripts": {
  "start": "react-app-rewired start",
  "build": "react-app-rewired build",
  "test": "react-app-rewired test",
  "eject": "react-app-rewired eject"
}
```
