// const injectBabelPlugin = require('react-app-rewired')

// const override = (config, env) => {
//   config = injectBabelPlugin(
//     [
//       'import',
//       {
//         libraryName: 'antd',
//         libraryDirectory: 'es',
//         style: 'css'
//       }
//     ],
//     config
//   )
//
//   return config
// }
//
// module.exports = override

const path = require('path')
const { override, fixBabelImports, addLessLoader, addWebpackAlias } = require('customize-cra')

module.exports = override(
  addWebpackAlias({
    '@': path.resolve(__dirname, 'src'),
    '@components': path.resolve(__dirname, 'src/components')
  }),
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: {
        '@primary-color': '#f9c700',
        '@link-color': '#f9c700'
      },
      cssModules: {
        localIdentName: '[local]--[hash:5]'
      }
    }
  })
)
